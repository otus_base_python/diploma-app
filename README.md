# Что это?

Веб-приложение :grin:

MyBirds - это ресурс для любителей попугаев :bird:, где они могут пообщаться с единомышленниками и позадавать вопросы.

# Запуск

Необходимо перейти в директорию с приложением.

```shell
cd ./app
```

<details>
<summary>Традиционно</summary>

Необходимо создать и активировать виртуальное окружение и объявить необходимые переменные окружения. Остальную часть можно выполнить с помощью `make`.

```shell
python3 -m venv venv
source venv/bin/activate
export PROJECT_ENVIRONMENT=LOCAL
export $(cat dev.env | xargs )
```
Запуск проекта.
```shell
make local
```
</details>

<details>
<summary>docker-compose</summary>

Запуск контейнеров:
```shell
make compose-run
```
Применения миграция, инициализация юзеров:
```shell
make compose-init
```
</details>
