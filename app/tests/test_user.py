"""
.app/test/test_user.py
"""
# pylint: disable=invalid-name,line-too-long
from django.contrib.auth import get_user_model
from django.core.management import call_command
from django.test import TestCase


class UsersManagersTests(TestCase):
    """
    Testing User Manager
    """

    @classmethod
    def setUpTestData(cls):
        """
        We would like to use one init data for all cases across that testcase
        ref: https://stackoverflow.com/questions/29428894/django-setuptestdata-vs-setup
        """
        call_command("init_groups")

    def test_create_user(self):
        """
        test_create_user
        """
        User = get_user_model()
        user = User.objects.create_user(email="normal@user.com", username="foo", password="bar")
        self.assertEqual(user.email, "normal@user.com")
        self.assertEqual(user.username, "foo")
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        try:
            self.assertEqual(user.username, "foo")
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email="")

    def test_create_superuser(self):
        """
        test_create_superuser
        """
        User = get_user_model()
        admin_user = User.objects.create_superuser(email="super@user.com", username="spam", password="eggs")
        self.assertEqual(admin_user.email, "super@user.com")
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        try:
            # username is None for the AbstractUser option
            # username does not exist for the AbstractBaseUser option
            self.assertEqual(admin_user.username, "spam")
        except AttributeError:
            pass
