"""
./app/test/test_first.py
"""
from django.core.management import call_command
from django.template.response import TemplateResponse
from django.test import TestCase

from community.user.models import User


class TestIndexPage(TestCase):
    """
    Запросы к index_page
    """

    # pylint: disable=invalid-name
    def setUp(self):
        """Инициализация юзера для тестов"""
        # redirect management command stdout to /dev/null
        # with open("/dev/null", "w") as f:
        #     sys.stdout = f

        # Init groups
        call_command("init_groups")

        self.user_data = {
            "username": "test",
            "password": "password",
            "email": "test@example.com",
        }
        self.user = User.objects.create_user(**self.user_data)

        # ---
        # Create and delete fake table
        # ref: https://stackoverflow.com/questions/7020966/how-to-create-table-during-django-tests-with-managed-false

    #  raise NotSupportedError(django.db.utils.NotSupportedError:
    #  SQLite schema editor cannot be used while foreign key constraint checks are enabled.
    #  Make sure to disable them before entering a transaction.atomic()
    #  context because SQLite does not support disabling them in the middle of a multi-statement transaction.

    def test_index_status_code_is_ok(self):
        """Проверяется что код ответа - 200"""
        response: TemplateResponse = self.client.get("/")
        self.assertEqual(response.status_code, 200)

    def test_health_check(self):
        """Проверяется что на странице хелсчека мы получаем 200 и нужный ответ"""
        response = self.client.get("/ht/")
        self.assertEqual(response.status_code, 200)
        self.assertInHTML("<td>working</td>", response.rendered_content)

    def test_anonymous_user(self):
        """
        Проверяется что в контексте анонимный пользователь, и он получает соотвествующее привествие.
        """
        response: TemplateResponse = self.client.get("/")
        user_instance = response.context["user"]
        self.assertTrue(user_instance.is_anonymous)
        self.assertInHTML('<p class="text-success fs-2">Hello, Stranger !</p>', response.rendered_content)

    def test_is_custom_user_exist(self):
        """
        Some test
        """
        # в контексте не тот юзер которого мы ожидаем :(
        self.client.login(**self.user_data)
        response = self.client.get("/")
        user = response.context["user"].username
        print(f"\n\n\nuser=<{user}>\n\n\n")
        u = User.objects.first()
        self.assertEqual(u, self.user)
