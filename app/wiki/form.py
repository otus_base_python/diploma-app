"""
./app/community/forms.py
"""

from django import forms
from .models import Parrot


class ParrotForm(forms.ModelForm):
    # pylint: disable=too-few-public-methods
    """
    Post form for community.parrot - model
    """

    class Meta:
        # pylint: disable=missing-class-docstring, too-few-public-methods
        model = Parrot
        fields = (
            "title",
            "text",
        )
