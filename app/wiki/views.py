"""
./app/community/views.py
"""
# pylint: disable=too-few-public-methods
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView

from .models import Parrot
from .form import ParrotForm


class ParrotsListView(ListView):
    """
    List view with current package model and URL dispatcher
    Template `./app/community/templates/wiki/parrot_list.html`
    """

    model = Parrot
    queryset = Parrot.objects.only("title")
    login_url = "login"
    redirect_field_name = "parrots"


class ParrotDetail(DetailView):
    """
    Detail view with current package model and URL dispatcher
    Template `./app/community/templates/community/parrot_detail.html`
    """

    model = Parrot


class ParrotCreate(PermissionRequiredMixin, CreateView):
    """
    Create Generic View (embedded form), pop parrot_author from form data, requires permissions.
    Current package model and URL dispatcher.
    Template `./app/community/templates/wiki/parrot_form.html`
    Ref in ParrotsListView Template.
    """

    permission_required = "wiki.add_parrot"
    model = Parrot
    success_url = reverse_lazy("parrots_wiki")
    form_class = ParrotForm

    def form_valid(self, form):
        """
        Parrot creator == author
        """
        form.instance.wiki_author = self.request.user
        return super().form_valid(form)


class ParrotUpdate(PermissionRequiredMixin, UpdateView):
    """
    Update Generic View.
    Current package model and URL dispatcher.
    Template `./app/community/templates/wiki/parrot_form.html`
    Ref in ParrotDetailView Template.
    """

    model = Parrot
    permission_required = "wiki.change_parrot"
    fields = (
        "title",
        "text",
    )

    def get_success_url(self):
        """
        Redirect back to the current article
        """
        return reverse_lazy("parrots_wiki_detail", kwargs={"pk": self.object.id})


class ParrotDelete(PermissionRequiredMixin, DeleteView):
    """
    Delete Generic View.
    Current package model and URL dispatcher.
    Template `./app/community/templates/wiki/parrot_confirm_delete.html`
    Ref in ParrotDetailView Template.
    """

    model = Parrot
    permission_required = "wiki.delete_parrot"
    success_url = reverse_lazy("parrots_wiki")
    fields = "__all__"
