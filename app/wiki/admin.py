"""
./app/wiki/admin.py
"""
from django.db import models
from django.contrib import admin

from martor.widgets import AdminMartorWidget
from martor.models import MartorField

from .models import Parrot, ParrotMeta


class ParrotMetaAdminInline(admin.TabularInline):
    # pylint: disable=too-few-public-methods
    """
    Django Markdown Editor - martor in Django Admin
    ref: https://github.com/agusmakmun/django-markdown-editor
    """
    model = ParrotMeta


class ParrotAdmin(admin.ModelAdmin):
    # pylint: disable=missing-class-docstring,too-few-public-methods
    inline = [
        ParrotMetaAdminInline,
    ]
    list_display = ["title", "id"]
    formfield_overrides = {
        MartorField: {"widget": AdminMartorWidget},
        models.TextField: {"widget": AdminMartorWidget},
    }


admin.site.register(Parrot, ParrotAdmin)
