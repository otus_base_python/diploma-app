"""
./app/wiki/models.py
"""
# pylint: disable=too-few-public-methods
from django.db import models
from django.conf import settings

from martor.models import MartorField


class Parrot(models.Model):
    # pylint: disable=line-too-long
    """
    Wiki parrot model
    """

    title = models.CharField(max_length=64)
    text = MartorField(blank=True)
    wiki_author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="wiki_author")

    def __str__(self):
        return f"ParrotName={self.title}"


class ParrotMeta(models.Model):
    # pylint: disable=missing-class-docstring
    parrot = models.ForeignKey(Parrot, on_delete=models.DO_NOTHING)
    text = MartorField()

    def __str__(self):
        return f"ParrotMeta=<{self.text}"
