"""
./app/wiki/apps.py
"""
from django.apps import AppConfig

# pylint: disable=locally-disabled, too-few-public-methods, missing-class-docstring


class WikiConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "wiki"
