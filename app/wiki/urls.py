"""
./app/wiki/urls.py
"""
from django.urls import path
from . import views

urlpatterns = [
    # CRUD
    path("parrots/", views.ParrotsListView.as_view(), name="parrots_wiki"),
    path("parrot/create/", views.ParrotCreate.as_view(), name="parrots_wiki_create"),
    path(
        "parrot/detail/<int:pk>/",
        views.ParrotDetail.as_view(),
        name="parrots_wiki_detail",
    ),
    path("parrot/update/<int:pk>/", views.ParrotUpdate.as_view(), name="parrots_wiki_update"),
    path("parrot/delete/<int:pk>/", views.ParrotDelete.as_view(), name="parrots_wiki_delete"),
]
