#!/bin/bash

# PATH_TO_PROJECT_ROOT=$1
CUR_DIR=$(pwd)

echo "Need to been in direcretory app, you are in ${CUR_DIR}"

source dev.env

export PGDATABASE=${POSTGRES_DB}
export PGUSER=${POSTGRES_USER}
export PGPASSWORD=${POSTGRES_PASSWORD}
export PGHOST=0.0.0.0
export PGPORT=5432

echo "Fetching dump from s3"

aws s3 cp s3://my-birds/pg_backup/pg_dump.sql backup.sql --profile karma-it-aws

echo "Applying backup on postgres"

# pg_restore --data-only ${PGDATABASE} < backup.sql
pg_restore --dbname ${PGDATABASE} < backup.sql

rm -rf backup.sql

echo "Done!"
