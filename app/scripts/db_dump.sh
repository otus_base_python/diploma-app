#!/bin/bash

# PATH_TO_PROJECT_ROOT=$1
CUR_DIR=$(pwd)

echo "Need to been in direcretory app, you are in ${CUR_DIR}"

source dev.env

export PGDATABASE=${POSTGRES_DB}
export PGUSER=${POSTGRES_USER}
export PGPASSWORD=${POSTGRES_PASSWORD}
export PGHOST=0.0.0.0
export PGPORT=5432

# pg_dump > pg_dump.sql

pg_dump -Fc ${PGDATABASE} > pg_dump.sql

aws s3 cp pg_dump.sql s3://my-birds/pg_backup/ --profile karma-it-aws

rm -rf pg_dump.sql

echo "End!"
