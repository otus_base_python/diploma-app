#!/bin/bash

python3 manage.py migrate
python3 manage.py init_groups

python3 manage.py shell -c \
"from community.user.models import User; import os; \
    User.objects.create_superuser('admin', 'admin@example.com', os.environ['DJANGO_ADMIN_PW'])"

python3 manage.py loaddata parrots.json
