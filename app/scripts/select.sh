#!/bin/bash

source ../dev.env

DB_CONTAINER_NAME='db'
TABLE_NAME=$1

docker exec -it $(docker ps --filter "name=${DB_CONTAINER_NAME}" --format '{{.ID}}') psql -c SELECT * FROM ${TABLE_NAME}; 


DB_STATUS=$(curl --silent -X GET -H "Accept: application/json" http://0.0.0.0:8080/ht/ | tac | tac | jq -r '.DatabaseBackend')

if [[ "$DB_STATUS" == "working" ]]; then
  echo "Everything is okay"
else
  echo "DB is unhealthy"
fi
