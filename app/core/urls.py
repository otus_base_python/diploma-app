"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# pylint: disable ungrouped-imports
from django.urls import path
from django.conf.urls import url, include
from .settings import DEBUG

urlpatterns = [
    path("", include("community.urls")),
    path("wiki", include("wiki.urls")),
    # auth
    path("accounts/", include("django.contrib.auth.urls")),
    url(r"^ht/", include("health_check.urls")),
    path("martor/", include("martor.urls")),
]

# import debug-toolbar if web-server mode == DEBUG
# if DEBUG:
#     import debug_toolbar
#
#     urlpatterns.append(
#         path("__debug__/", include(debug_toolbar.urls)),
#     )
