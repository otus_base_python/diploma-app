"""
./app/community/apps.py
"""
# pylint: disable=locally-disabled, too-few-public-methods
from django.apps import AppConfig


class CommunityConfig(AppConfig):
    """
    Config for INSTALLED_APPS
    """

    default_auto_field = "django.db.models.BigAutoField"
    name = "community"
