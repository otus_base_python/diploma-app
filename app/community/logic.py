"""
./app/community/logic.py
extra logic for views
"""


def gravatar(email_hash: str, size=100, default="identicon", rating="g") -> str:
    """
    gravatar url builder
    """
    url = "https://secure.gravatar.com/avatar"
    return f"{url}/{email_hash}?s={size}&d={default}&r={rating}"
