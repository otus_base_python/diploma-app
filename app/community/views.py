"""
./app/community/views.py
"""
# pylint: disable=too-few-public-methods
from django.urls import reverse_lazy
from django.views.generic import (
    CreateView,
    ListView,
    DetailView,
    UpdateView,
    DeleteView,
)
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
    PermissionRequiredMixin,
    UserPassesTestMixin,
)

from community.user.models import User
from community.user.forms import CustomUserCreationForm
from .models import Article
from .logic import gravatar
from .form import ArticleForm


class UserRegistration(CreateView):
    """
    Form `./app/community/user/forms.py`
    Model `./app/community/user/models.py`
    URL dispatcher `./app/community/urls.py`
    Template `./app/community/templates/community_user/user_form.html`
    Login Redirect `./app/core/settings.py`
    """

    model = User
    success_url = reverse_lazy("login")
    form_class = CustomUserCreationForm


class ArticlesListView(ListView):
    """
    List view with current package model and URL dispatcher
    Template `./app/community/templates/community/article_list.html`
    """

    model = Article
    queryset = Article.objects.only("title", "author__email").select_related("author")
    login_url = "login"
    redirect_field_name = "articles"


class ArticleCreate(PermissionRequiredMixin, CreateView):
    """
    Create Generic View (embedded form), pop article_author from form data, requires permissions.
    Current package model and URL dispatcher.
    Template `./app/community/templates/community/article_form.html`
    Ref in ArticlesListView Template.
    """

    permission_required = "community.add_article"
    model = Article
    success_url = reverse_lazy("articles")  # TODO redirect to detail of this post?
    form_class = ArticleForm

    def form_valid(self, form):
        """
        Article creator == author
        """
        form.instance.author = self.request.user
        return super().form_valid(form)


class ArticleDetail(DetailView):
    """
    Detail view with current package model and URL dispatcher
    Template `./app/community/templates/community/article_detail.html`
    """

    model = Article
    permission_required = "community.view_article"
    queryset = Article.objects.only("title", "text", "author__username", "author__avatar_hash").select_related(
        "author"
    )

    def get_context_data(self, **kwargs):
        """
        Throw avatar_url (gravatar) into context
        """
        context = super().get_context_data(**kwargs)
        avatar_url: str = gravatar(email_hash=self.queryset.first().author.avatar_hash)
        context["avatar_url"] = avatar_url
        return context


class ArticleTestMixin(LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin):
    """
    DRY staff.
    Test for edit/delet buttons in ArticleDetail, \
        because only post author or personal can edit/delete article.
    """

    model = Article
    lookup = "pk"

    def test_func(self):
        """
        Check user and his groups
        """
        article_author_username = (
            self.model.objects.filter(id=self.kwargs[self.lookup])
            .only("author__username")
            .select_related("author")
            .first()
            .author.username
        )
        current_username = self.request.user.username
        is_author = bool(article_author_username == current_username)
        is_moderator = self.request.user.groups.filter(name="Moderator").exists()
        print(is_author, is_moderator)  # TODO add logging
        return is_author or is_moderator


class ArticleUpdate(ArticleTestMixin, UpdateView):
    """
    Update Generic View (embeeded form),
    Current package model and URL dispatcher.
    Template `./app/community/templates/community/article_form.html`
    Ref in ArticleDeletetView Template.
    """

    model = Article
    permission_required = "community.change_article"
    fields = (
        "title",
        "text",
    )

    def get_success_url(self):
        """
        Redirect back to the current article
        """
        return reverse_lazy("article_detail", kwargs={"pk": self.object.id})


class ArticleDelete(ArticleTestMixin, DeleteView):
    """
    Delete Generic View.
    Current package model and URL dispatcher.
    Template `./app/community/templates/community/article_confirm_delete.html`
    Ref in ArticleDeletetView Template.
    """

    model = Article
    permission_required = "community.delete_article"
    success_url = reverse_lazy("articles")
    fields = "__all__"
