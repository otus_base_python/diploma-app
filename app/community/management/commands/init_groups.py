"""
.app/community/management/commands/init_groups.py
Provision Common User Groups for Community
How to use?
python manage.py init_groups
"""
from django.core.management import BaseCommand
from django.contrib.auth.models import Group, Permission

from community import models
import wiki.models as wiki

GROUPS_PERMISSIONS = {
    "UnConfirmed": {
        models.Article: ["view"],
        models.Tag: ["view"],
    },
    "Author": {
        models.Article: ["add", "change", "view"],
        models.Tag: ["add", "change", "view"],
    },
    "Moderator": {
        models.Article: ["add", "change", "delete", "view"],
        wiki.Parrot: ["add", "change", "delete", "view"],
        models.Tag: ["add", "change", "delete", "view"],
    },
}


class Command(BaseCommand):
    # pylint: disable=useless-super-delegation,too-few-public-methods,unused-argument,protected-access
    """
    Provision common groups
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    help = "Create default groups"

    def handle(self, *args, **options):
        """
        Looping trough perm elements
        """

        for group_name in GROUPS_PERMISSIONS:

            # Get or create group
            group, _ = Group.objects.get_or_create(name=group_name)

            # Loop models in group
            for model_cls in GROUPS_PERMISSIONS[group_name]:

                # Loop permissions in group/model
                for _, perm_name in enumerate(GROUPS_PERMISSIONS[group_name][model_cls]):

                    # Generate permission name as Django would generate it
                    codename = perm_name + "_" + model_cls._meta.model_name

                    try:
                        # Find permission object and add to group
                        perm = Permission.objects.get(codename=codename)
                        group.permissions.add(perm)
                        self.stdout.write("Adding " + codename + " to group " + group.__str__())
                    except Permission.DoesNotExist:
                        self.stdout.write(codename + " not found")
