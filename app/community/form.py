"""
./app/community/forms.py
"""

from django import forms
from .models import Article


class ArticleForm(forms.ModelForm):
    # pylint: disable=too-few-public-methods
    """
    Post form for community.article - model
    """

    class Meta:
        # pylint: disable=missing-class-docstring, too-few-public-methods
        model = Article
        fields = (
            "title",
            "text",
        )
