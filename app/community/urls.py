"""
./app/community/urls.py
"""
from django.urls import path, include
from django.contrib import admin
from django.views.generic import TemplateView
from . import views

urlpatterns = [
    path("", TemplateView.as_view(template_name="community/index.html"), name="index"),
    # admin
    path("admin/", admin.site.urls),
    # auth
    path("registration/", views.UserRegistration.as_view(), name="registration"),
    # CRUD
    path("articles/", views.ArticlesListView.as_view(), name="articles"),
    path("article/create/", views.ArticleCreate.as_view(), name="article_create"),
    path(
        "articles/detail/<int:pk>/",
        views.ArticleDetail.as_view(),
        name="article_detail",
    ),
    path("article/update/<int:pk>/", views.ArticleUpdate.as_view(), name="article_update"),
    path("article/delete/<int:pk>/", views.ArticleDelete.as_view(), name="article_delete"),
    path("accounts/", include("allauth.urls")),
]
