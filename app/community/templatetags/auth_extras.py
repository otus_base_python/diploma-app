"""
./app/community/templatetags/auth_extras.py
Now we can do something like this in our templates:
{% if request.user|has_group:"Moderator" %}
  Show Secret Data
{% else %}
  You shall not pass!
{% endif %}
"""
from django import template
from django.contrib.auth.models import Group

register = template.Library()


@register.filter(name="has_group")
def has_group(user, group_name):
    """
    Has group checker
    """
    group = Group.objects.get(name=group_name)
    return bool(group in user.groups.all())
