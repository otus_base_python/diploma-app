"""
./app/community/user/admin.py
"""
from django.db import models
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from martor.widgets import AdminMartorWidget
from martor.models import MartorField

from community.models import Profile, Article, ArticleMeta, Tag
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import User


class CustomUserAdmin(UserAdmin):
    # pylint: disable=too-few-public-methods
    """
    Replace standart UI logic in admin
    """

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = User
    list_display = (
        "email",
        "username",
        "is_staff",
        "is_active",
        "is_superuser",
    )
    list_filter = (
        "email",
        "username",
        "is_staff",
        "is_active",
        "is_superuser",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "email",
                    "username",
                    "password",
                    "groups",
                    "user_permissions",
                    "is_superuser",
                )
            },
        ),
        ("Permissions", {"fields": ("is_staff", "is_active")}),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("email", "password1", "password2", "is_staff", "is_active"),
            },
        ),
    )
    search_fields = ("email",)
    ordering = ("email",)


# Register our models in django admin
admin.site.register(User, CustomUserAdmin)
admin.site.register(Profile)
admin.site.register(Tag)


class ArticleMetaAdminInline(admin.TabularInline):
    # pylint: disable=too-few-public-methods
    """
    Django Markdown Editor - martor in Django Admin
    ref: https://github.com/agusmakmun/django-markdown-editor
    """
    model = ArticleMeta


class ArticleAdmin(admin.ModelAdmin):
    # pylint: disable=missing-class-docstring,too-few-public-methods
    inline = [
        ArticleMetaAdminInline,
    ]
    list_display = ["title", "id"]
    formfield_overrides = {
        MartorField: {"widget": AdminMartorWidget},
        models.TextField: {"widget": AdminMartorWidget},
    }


admin.site.register(Article, ArticleAdmin)
