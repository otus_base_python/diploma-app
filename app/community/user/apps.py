"""
.app/community/user/apps.py

!Note: UserConfig clas also imported in __init__ for current dirxw
"""
# pylint: disable=locally-disabled, too-few-public-methods
from django.apps import AppConfig


class UserConfig(AppConfig):
    """
    config
    """

    # default_auto_field = 'django.db.models.BigAutoField' #  TODO is it needed?
    name = "community.user"
    label = "community_user"
