"""
./app/community/models.py
"""
# pylint: disable=too-few-public-methods,unused-argument
from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import Group
from martor.models import MartorField


class Profile(models.Model):
    """
    Profile for our User (./app/community/user/models.py model=<User>
    """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)

    def __str__(self):
        return f"ProfileFor=<{self.user}"


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(sender, instance, created, **kwargs):
    """
    Create user profile instance and \
        auto assign Group Author `./app/community/management/commands/init_groups.py` to user.
    """
    if created:
        Profile.objects.create(user=instance)
        group = Group.objects.get(name="Author")
        group.user_set.add(instance)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def save_user_profile(sender, instance, **kwargs):
    """
    Save instance
    """
    instance.profile.save()


class Article(models.Model):
    # pylint: disable=line-too-long
    """
    Community article model, many-to-one relation with author(user).
    """

    title = models.CharField(max_length=64)
    text = MartorField(blank=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="authors")

    def __str__(self):
        return f"ArticleTitle={self.title},Author={self.author}"


class ArticleMeta(models.Model):
    """
    Helper for Article model in django admin with martor
    """

    article = models.ForeignKey(Article, on_delete=models.DO_NOTHING)
    text = MartorField()

    class Meta:
        # pylint: disable=missing-class-docstring
        managed = False
        db_table = "community_articlemeta"

    def __str__(self):
        return f"ArticleMeta=<{self.text}"


class Tag(models.Model):
    """
    Community tag model, many-to-many relation with article.
    """

    name = models.CharField(max_length=64)
    articles = models.ManyToManyField(Article)

    def __str__(self):
        return f"TagName={self.name}"
